resource "aws_iam_role" "spinnaker_role" {
    name = "spinnaker_role"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal":
                {
                    "Service": "ec2.amazonaws.com"
                }
        }
    ]
}

EOF
}

resource "aws_iam_policy_attachment" "spinnaker_role_policy_att" {
    name = "spinnaker_user_policy_att"
    roles = ["${aws_iam_role.spinnaker_role.name}"]
    policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
}

resource "aws_iam_role_policy" "spinnaker_role_policy" {
    name = "spinnaker-role-policy"
    role = "${aws_iam_role.spinnaker_role.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468442506000",
            "Effect": "Allow",
            "Action": [
                "iam:PassRole"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "spinnaker_profile" {
    name = "spinnaker_profile"
    roles = ["${aws_iam_role.spinnaker_role.name}"]
}

resource "aws_instance" "spinnaker" {
    depends_on = ["aws_internet_gateway.gateway"]

    ami = "ami-8234fbef"
    instance_type = "m4.xlarge"
    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
    associate_public_ip_address = true
    iam_instance_profile = "${aws_iam_instance_profile.spinnaker_profile.id}"
    key_name = "${aws_key_pair.default.key_name}"

    tags {
        Name = "spinnaker"
    }
}
