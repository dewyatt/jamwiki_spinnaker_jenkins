resource "aws_iam_role" "base_iam_role" {
    name = "BaseIAMRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal":
                {
                    "Service": "ec2.amazonaws.com"
                }
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "baseiam_profile" {
    name = "${aws_iam_role.base_iam_role.name}"
    roles = ["${aws_iam_role.base_iam_role.name}"]
}

resource "aws_iam_user" "spinnaker_user" {
    name = "spinnaker"
}

resource "aws_iam_user_policy" "spinnaker_user_policy" {
    name = "spinnaker-user-policy"
    user = "${aws_iam_user.spinnaker_user.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468442506000",
            "Effect": "Allow",
            "Action": [
                "iam:PassRole"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "spinnaker_user_policy_att" {
    name = "spinnaker_user_policy_att"
    users = ["${aws_iam_user.spinnaker_user.name}"]
    policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
}
