resource "aws_security_group" "aptlyrepo" {
    vpc_id = "${aws_vpc.default.id}"
    name = "aptlyrepo"
    description = "aptly repo via nginx"


    ingress {
        from_port = 9999
        to_port = 9999
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }

}

resource "aws_instance" "jenkins" {
    depends_on = ["aws_internet_gateway.gateway"]
    # Ubuntu Server 14.0 LTS HVM
    ami = "ami-2d39803a"
    instance_type = "t2.small"
    subnet_id = "${aws_subnet.default.id}"
    vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}", "${aws_security_group.aptlyrepo.id}"]
    associate_public_ip_address = true
    key_name = "${aws_key_pair.default.key_name}"

    tags {
        Name = "jenkins"
    }
}
