resource "aws_vpc" "default" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true

    tags {
        Name = "defaultvpc"
    }
}

resource "aws_internet_gateway" "gateway" {
    vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route" "internet_route" {
    route_table_id = "${aws_vpc.default.main_route_table_id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
}

resource "aws_subnet" "default" {
    depends_on = ["aws_internet_gateway.gateway"]

    vpc_id = "${aws_vpc.default.id}"
    cidr_block = "10.0.0.0/24"
    availability_zone = "us-east-1b"
    map_public_ip_on_launch = true

    tags {
        Name = "default.internal.us-east-1"
    }
}

resource "aws_subnet" "alt" {
    depends_on = ["aws_internet_gateway.gateway"]

    vpc_id = "${aws_vpc.default.id}"
    cidr_block = "10.0.1.0/24"
    availability_zone = "us-east-1c"
    map_public_ip_on_launch = true

    tags {
        Name = "default.internal.us-east-1-alt"
    }
}

resource "aws_security_group" "allow_ssh" {
    vpc_id = "${aws_vpc.default.id}"
    name = "allow_ssh"
    description = "Allow SSH from any"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        self = true
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
