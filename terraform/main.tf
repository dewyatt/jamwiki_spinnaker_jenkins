provider "aws" {
    # hardcoded because I'm avoiding creating maps for AMIs
    region = "us-east-1"
}

variable "db_username" {}
variable "db_password" {}

resource "aws_key_pair" "default" {
    key_name = "my-aws-account-keypair"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxOvS1jMowkEfW3no08UataB86Qxms46k5k4cmicTtu8k6KK/L6kdsONSmoUhSr6ea75qIEZYRMrAUxoeu2/xVeqnHHGeANcXSG7PFAJbc3nf9j1VqM2wAw/yeTtffejj6tMqS38TXolHYhVmMdhwVnB0sXIyIVAgSSh02NVVmFt9aIGt/E//Eg7FtCVNZbw8pP9gQVNV/HoqL61MpJB+TL8dJD0lS5uhUvN7ToVb/PF97/Hd2cVPLwO/x5SAJvzxXybYJNBlT4pcuPTUy2tNLAicuc5jY8SRuO25W/CyXysMAtYqZRRf0XuLGSdJvF61qQHhkJEQv88YEoemhN2zb"
}
